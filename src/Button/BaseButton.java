package Button;

import Mode.Mode;
import Mode.ButtonModeChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BaseButton extends JButton implements MouseListener {
    public  Mode mode;


    public BaseButton(){
        super();


    }

    public BaseButton(ImageIcon icon, int x , int y){
        super(icon);
        super.setBounds(x,y,100,100);
        int offset =super.getInsets().left;
        this.setBackground(Color.WHITE);
        super.setIcon(resizeIcon(icon,this.getWidth()-offset,this.getHeight() - offset));
        this.addMouseListener(this);
    }
    private Icon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }



    @Override
    public void mouseClicked(MouseEvent e) {
        ButtonModeChooser chooser = new ButtonModeChooser(e);


    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }



}
