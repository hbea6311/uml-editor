package Button;

import Mode.CreateLineMode;

import javax.swing.*;

public class CompositionButton extends BaseButton{
    public CompositionButton(){

        super(new ImageIcon("image/composition.jpg"),0,0);
        mode = new CreateLineMode("Composition");
    }
}
