package Button;

import Mode.SelectMode;

import javax.swing.*;

public class SelectButton extends BaseButton{
    public SelectButton(){
        super(new ImageIcon("image/selection.jpg"),0,0);
        mode = new SelectMode();
    }
}
