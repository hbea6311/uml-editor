package Button;


import Mode.CreateLineMode;

import javax.swing.*;

public class GeneralizeButton extends BaseButton{
    public GeneralizeButton(){
        super(new ImageIcon("image/generalization.jpg"),0,0);
        mode = new CreateLineMode("Generalization");
    }
}
