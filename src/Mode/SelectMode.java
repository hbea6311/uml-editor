package Mode;

import Canvas_Component.Component;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;

public class SelectMode extends Mode{
    private List<Component> allCanvasComponent =null;
    private Point startP;
    private String judgeInside;

    @Override
    public void mousePressed(MouseEvent e) {

        startP = e.getPoint();
        allCanvasComponent = canvas.getAllCanvasComponent();

        //clear previous selected objects
        canvas.resetCanvas();

        //show point if clicked point is in object
        for(int i = allCanvasComponent.size()-1; i>=0; i--){
            Component component = allCanvasComponent.get(i);
            judgeInside = component.inside(e.getPoint());
            if(judgeInside != null){
                canvas.selectedComponent = component;
                break;
            }
        }
        canvas.repaint();
    }
    @Override
    public void mouseDragged(MouseEvent e){
        int moveX = e.getX()-startP.x;
        int moveY = e.getY()- startP.y;
        //if something has been selected
        if(canvas.selectedComponent != null){
            if(judgeInside !=null) {// if mouse in area
                canvas.selectedComponent.resetPosition(moveX, moveY);
                startP.x=e.getX();
                startP.y=e.getY();
            }

        }
        //以矩形進行選取，並設定起始點
        else{
            if (e.getX() > startP.x)
                canvas.selectedArea.setBounds(startP.x, startP.y, Math.abs(moveX), Math.abs(moveY));
            else
                canvas.selectedArea.setBounds(e.getX(), e.getY(), Math.abs(moveX), Math.abs(moveY));


        }


        canvas.repaint();
    }
    @Override
    public void mouseReleased(MouseEvent e){
        //計算畫出的大小
        canvas.selectedArea.setSize(Math.abs(e.getX() - startP.x), Math.abs(e.getY() - startP.y));
        canvas.repaint();
    }

}
