package Mode;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;

import Canvas_Component.Component;
import Canvas_Component.Line;

public class CreateLineMode extends Mode{
    private String lineMode;
    private ObjectFactory factory = new ObjectFactory();
    private Point startP;
    private int portIndex_f = -1;
    private int portIndex_e = -1;
    private Component component_f = null;
    private Component component_e = null;
    private List<Component> components;

    public CreateLineMode(String lineMode){
        this.lineMode = lineMode;
    }
    public void mousePressed(MouseEvent e){
        components = canvas.getAllCanvasComponent();
        //找到被點及的物件與port
        startP = findConnectCom(e.getPoint(),"from");

    }
    public void mouseDragged(MouseEvent e){
        if(startP != null){
            Line line = factory.createLine(lineMode,startP,e.getPoint());
            canvas.tempLine = line;
            canvas.repaint();
        }
    }
    public void mouseReleased(MouseEvent e) {
        Point endP = null;
        if (startP != null) {
            //找到被點及的物件與port
            endP = findConnectCom(e.getPoint(), "end");

            /* if end of line inside the basic object */
            if (endP != null) {
                Line line = factory.createLine(lineMode, startP, endP);
                canvas.addShape(line);

                /* add relative ports to line */
                line.setPorts(component_f.getPort(portIndex_f), component_e.getPort(portIndex_e));

                /* add line to relative port of two basic object */
                component_f.getPort(portIndex_f).addLine(line);
                component_e.getPort(portIndex_e).addLine(line);
            }
            // reset
            canvas.tempLine = null;
            canvas.repaint();
            startP = null;
        }
    }


    public Point findConnectCom(Point p, String target){
       for(int i=0;i<components.size();i++){
           Component component = components.get(i);

           //check if this component is selected
           int portIndex =0;
           String judgeInside = component.inside(p);
           if(judgeInside !=null) {

               if(judgeInside == "insideGroup"){
                   component = component.getSelectedComp();
                   portIndex = Integer.parseInt(component.inside(p));
               }
               else{
                   portIndex = Integer.parseInt(judgeInside);
               }

               switch (target) {
                   case "from":
                       component_f = component;
                       portIndex_f = portIndex;
                       break;
                   case "end":
                       component_e = component;
                       portIndex_e = portIndex;
                       break;
               }
               Point portLocation = new Point();
               portLocation.setLocation(component.getPort(portIndex).getCenterX(), component.getPort(portIndex).getCenterY());
               return portLocation;
           }
       }
       return null;
    }





}


