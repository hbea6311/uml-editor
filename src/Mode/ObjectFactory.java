package Mode;

import Canvas_Component.*;

import java.awt.*;

public class ObjectFactory extends Mode{
    public BasicComponent createObject(String objectType , Point p){
        System.out.println("shapeFactory was called");
        if(Mode.mode == ("Class")){
            System.out.println("new object generate");
            return new ClassComponent(p.x,p.y);
        }
        else if(Mode.mode == "UseCase"){
            return new UseCaseComponent(p.x, p.y);
        }
        return null;
    }
    public Line createLine(String lineType, Point startP, Point endP){
        if(Mode.mode == ("Association")){
            return new AssociationLine(startP.x, startP.y, endP.x, endP.y);
        }
        if(Mode.mode == ("Generalization")){
            return new GeneralizationLine(startP.x, startP.y, endP.x, endP.y);
        }
        if(Mode.mode == ("Composition")){
            return new CompositionLine(startP.x, startP.y, endP.x, endP.y);
        }
        return null;
    }

}
