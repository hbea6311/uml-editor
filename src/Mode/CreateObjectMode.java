package Mode;

import Canvas_Component.*;


import java.awt.event.MouseEvent;

public class CreateObjectMode extends Mode{
    private ObjectFactory creator = new ObjectFactory();
    public CreateObjectMode(){}

    @Override
    public void mousePressed(MouseEvent e){

        BasicComponent basicObject = creator.createObject(Mode.mode,e.getPoint());

        canvas.addShape(basicObject);
        canvas.repaint();
    }
}
