package Mode;

import Button.*;

import java.awt.event.MouseEvent;

public class ButtonModeChooser {

    public ButtonModeChooser(){}
    public ButtonModeChooser(MouseEvent e){
        if (e.getSource() instanceof ClassButton){
            Mode.mode=("Class");

        }
        else if(e.getSource() instanceof UseCaseButton){
            Mode.mode=("UseCase");

        }
        else if(e.getSource() instanceof AssociationButton) {
            Mode.mode = ("Association");

        }
        else if(e.getSource() instanceof CompositionButton){
            Mode.mode = ("Composition");

        }
        else if(e.getSource() instanceof SelectButton){
            Mode.mode = ("Select");
        }
        else
        {
            Mode.mode = ("Generalization");
        }
        System.out.println("now is "+Mode.mode);

    }

}
