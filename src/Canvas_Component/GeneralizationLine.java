package Canvas_Component;

import java.awt.*;

public class GeneralizationLine extends Line{
    private int arrowW = 10, arrowH = 10;
    public GeneralizationLine(int x1,int y1, int x2, int y2){
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    @Override
    public void draw(Graphics g) {
        //畫直線
        g.drawLine(x1,y1,x2,y2);

        //計算前方箭頭的斜率
        int dx = x2-x1;
        int dy = y2-y1;
        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - arrowW, xn = xm, ym = arrowH, yn = -arrowH, x;
        double sin = dy/D, cos = dx/D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        //繪製三角形
        int[] xpoints = {x2, (int) xm, (int) xn};
        int[] ypoints = {y2, (int) ym, (int) yn};

        g.fillPolygon(xpoints, ypoints, 3);
    }

}
