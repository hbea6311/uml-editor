package Canvas_Component;

import java.awt.*;

public abstract class Component {
    public Point getPort;
    protected int x1,x2,y1,y2;
    public boolean groupSelected;

    public abstract void draw(Graphics g);

    public int getX1(){return x1;}
    public int getX2(){return x2;}
    public int getY1(){return y1;}
    public int getY2(){return y2;}

    public String inside(Point p){
        return null;
    }
    public void showPort(Graphics g){}
    public Port getPort(int portIndex){
        return null;
    }
    public void changeName(String name){}

    public  void resetPosition(int moveX,int moveY){}; //for basic object
    public  void resetPosition(){};  //for line


    //only for group
    public Component getSelectedComp(){
        return null;
    }
    public void resetSelectedComp(){};



}
