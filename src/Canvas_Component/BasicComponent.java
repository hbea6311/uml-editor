package Canvas_Component;

import java.awt.*;

public class BasicComponent extends Component {
    protected int width,height ;
    protected String objectName ="Object name";
    protected Font font= new Font(Font.SERIF, Font.BOLD, 14);
    protected Port[] ports = new Port[4];
    protected int offset = 5;

    @Override
    public void draw(Graphics g) {

    }
    @Override
    public void showPort(Graphics g){
        for(int i=0;i<ports.length;i++){
            g.fillRect(ports[i].x,ports[i].y,ports[i].width,ports[i].height);
        }
    }
    public Port getPort(int portIndex){
        return ports[portIndex];
    }

    //generate 4 ports for basicObject
    protected void createPorts(){
        int[] Xpoint = {(x1+x2)/2, x2+offset,(x1+x2)/2,x1-offset};
        int[] Ypoint = {y1-offset,(y1+y2)/2,y2+offset,(y1+y2)/2};

        for(int i =0;i < ports.length;i++){
            Port port = new Port();
            port.setPort(Xpoint[i],Ypoint[i],offset);
            ports[i]=port;

        }

    }

    public String inside(Point p){
        Point objectCenter = new Point();
        objectCenter.x=(x1+x2)/2;
        objectCenter.y=(y1+y2)/2;
        Point[] bounds = {new Point(x1,y1),new Point(x2,y1), new Point(x2,y2), new Point(x1,y2)};

        for(int i=0; i< bounds.length; i++){
            Polygon t = new Polygon();
            int secondIndex = ((i+1)%4);
            t.addPoint(bounds[i].x,bounds[i].y);
            t.addPoint(bounds[secondIndex].x,bounds[secondIndex].y);
            t.addPoint(objectCenter.x,objectCenter.y);

            if(t.contains(p)){
                System.out.println("in object area");
                return Integer.toString(i);

            }
        }
        return null;
    }

    public void resetPosition(int moveX,int moveY){
        int x1 = this.x1 +moveX;
        int y1 = this.y1+ moveY;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x1+width;
        this.y2 = y1+height;

        int[] xpoint = {(x1+x2)/2, x2 + offset, (x1+x2)/2, x1 - offset};
        int[] ypoint = {y1 - offset, (y1+y2)/2, y2+offset, (y1+y2)/2};

        for(int i = 0; i < ports.length; i++) {
            ports[i].setPort(xpoint[i], ypoint[i], offset);
            ports[i].resetLines();
        }
    }
    @Override
    public void changeName(String name){
        this.objectName = name;
    }
}
