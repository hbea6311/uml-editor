package Canvas_Component;

import java.awt.*;

public abstract class Line extends Component {
    protected Port[] ports = new Port[2];
    private String selectedFlag;
    public abstract void draw(Graphics g);

    public void setPorts(Port port_f, Port port_e){
        this.ports[0] = port_f;
        this.ports[1] = port_e;
    }

    public void resetPosition(){
        this.x1 = (int)ports[0].getCenterX();
        this.y1 = (int)ports[0].getCenterY();
        this.x2 = (int)ports[1].getCenterX();
        this.y2 = (int)ports[1].getCenterY();
    }
    public void resetFromEnd(Point p){
        if(selectedFlag == "start"){
            this.x1 = p.x;
            this.y1 = p.y;
        }
        else if(selectedFlag == "end") {
            this.x2 = p.x;
            this.y2 = p.y;
        }
    }

}
