package Canvas_Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Port extends Rectangle {
    private List<Line> lines = new ArrayList<>();

    public void setPort(int centerX, int centerY, int offset){
        int x = centerX - offset;
        int y = centerY - offset;
        int w = offset * 2;
        int h = offset * 2;
        setBounds(x, y, w, h);
    }
    public void addLine(Line line){
        lines.add(line);
    }
    public void resetLines(){
        for(int i=0;i<lines.size();i++){
            Line line = lines.get(i);
            line.resetPosition();
        }
    }



}
