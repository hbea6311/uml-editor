package Canvas_Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Group extends Component {

    private List<Component> components = new ArrayList<Component>();
    private Component selectedComp;


    @Override //呼叫group裡物件的繪製
    public void draw(Graphics g) {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            component.draw(g);
        }
    }
    public void addComponent(Component component){
        components.add(component);
    }
    @Override
    public String inside(Point p){
        for(int i =0;i<components.size();i++){
            Component component = components.get(i);
            String judgeInside = component.inside(p);
            System.out.println("in group component");
            if(judgeInside != null){
                selectedComp = component;
                return "insideGroup";
            }
        }
        return null;
    }
    @Override
    public Component getSelectedComp(){
        return selectedComp;
    }
    public void resetPosition(int moveX, int moveY){
        for(int i =0;i<components.size();i++){
            Component component = components.get(i);
            component.resetPosition(moveX, moveY);
        }
    }
    public List<Component> getGroupComponents(){
        return  components;
    }

    @Override
    public void resetSelectedComp() {
        selectedComp = null;
    }
}