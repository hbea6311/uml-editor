package Frame;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class MainFrame {
    private JFrame  frame;
    private JButton select_button,association_button;

    private JToolBar toolBar;
    private JPanel toolPanel;
    private Canvas canvas;


    public MainFrame() throws IOException {
        frame = new JFrame("UMLeditor");
        frame.setLayout(new BorderLayout());

        toolPanel = new ToolPanel().getPanel(); //set toolpanel
        frame.add(toolPanel,BorderLayout.WEST);

        frame.setJMenuBar(new Menubar().getMenulBar()); // set toolbar

        canvas = Canvas.getInstance(); // set canvas

        frame.add(canvas,BorderLayout.CENTER);




        frame.setSize(800,700);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }



}
