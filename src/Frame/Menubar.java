package Frame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class Menubar extends JMenuBar {
    JMenuBar menulBar = new JMenuBar();
    private Canvas canvas;
    public Menubar() {
        canvas = Canvas.getInstance();

        JMenu file = new JMenu("File");
        file.add(new JMenuItem("New UML"));
        file.add(new JMenuItem("save"));

        JMenu edit = new JMenu("edit");
        JMenuItem item = new JMenuItem("change name");
        item.addActionListener(new ChangeNameListner());
        edit.add(item);

        item = new JMenuItem("group");
        item.addActionListener(new GroupCompListner());
        edit.add(item);

        item = new JMenuItem("unGroup");
        item.addActionListener(new unGroupListner());
        edit.add(item);

        menulBar.add(file);
        menulBar.add(edit);
    }
    public JMenuBar getMenulBar(){
        return menulBar;

    }
    public void changeName(){
        JFrame inputTextFrame = new JFrame("Change Component Name");
		inputTextFrame.setSize(400, 100);
		inputTextFrame.getContentPane().setLayout(new GridLayout(0, 1));

        //input are panel
		JPanel panel = null;
        panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        JTextField Text =  new JTextField("Object Name");
		panel.add(Text);
		inputTextFrame.getContentPane().add(panel);

        //button area panel
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        JButton confirm = new JButton("OK");
		panel.add(confirm);

        JButton cancel = new JButton("Cancel");
		panel.add(cancel);

		inputTextFrame.getContentPane().add(panel);

		inputTextFrame.setLocationRelativeTo(null);
		inputTextFrame.setVisible(true);

		//註冊動作
		confirm.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            canvas.changeCompName(Text.getText());
            inputTextFrame.dispose();

        }
    });

		cancel.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            inputTextFrame.dispose();
        }
    });


}
    class ChangeNameListner implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            changeName();
        }
    }
    class GroupCompListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            canvas.groupComponent();
        }
    }
    class unGroupListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            canvas.unGroupComponent();
        }
    }
}
