package Frame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import Canvas_Component.Component;
import Canvas_Component.Group;
import Mode.Mode;

public class Canvas extends JPanel{
    private static Canvas instance =null; //for singleton
    private EventListener listener;
    public static Mode currentMode;
    private List<Component> allCanvasComponent = new ArrayList<Component>();
    public Component selectedComponent;
    public  Rectangle selectedArea = new Rectangle();
    public Component tempLine= null;

    public Canvas(){
        this.setBackground(Color.WHITE);

    }
    public static Canvas getInstance(){
        if(instance==null){
            instance = new Canvas();

        }
        return instance;
    }
    public void addShape(Component component){
        allCanvasComponent.add(component);
    }

    public List<Component> getAllCanvasComponent(){ return allCanvasComponent;}

    public void resetCanvas(){
        if(selectedComponent !=null) {
            selectedComponent = null;
        }
        selectedArea.setBounds(0,0,0,0);
    }
    public void changeCompName(String name){
        if(selectedComponent != null){
            selectedComponent.changeName(name);
            repaint();
        }
    }
    public void paint(Graphics g){

        /* set canvas area */
        Dimension dim = getSize();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, dim.width, dim.height);
        /* set painting color */
        g.setColor(Color.BLACK
        );
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(1));

        //paint object in List object
        if (allCanvasComponent.size()>0) {


            for (int i = allCanvasComponent.size() - 1; i >= 0; i--) {
                Component component = allCanvasComponent.get(i);
                component.draw(g);
                //預設為未選取
                component.groupSelected = false;
                if(selectedArea != null && checkSelectedArea(component)==true){
                    component.showPort(g);
                    component.groupSelected = true;
                }

            }
        }
        //paint dragging line
        if(tempLine !=null){
            tempLine.draw(g);
        }

        //
        if(selectedComponent !=null){
            selectedComponent.showPort(g);
        }

        if(selectedArea != null){
            int alpha = 85; // 33% transparent
            g.setColor(new Color(37, 148, 216, alpha));
            g.fillRect(selectedArea.x, selectedArea.y, selectedArea.width, selectedArea.height);
            g.setColor(new Color(37, 148, 216));
            g.drawRect(selectedArea.x, selectedArea.y, selectedArea.width, selectedArea.height);

        }

    }
    public void setCurrentMode(){
        removeMouseListener((MouseListener) listener);
        removeMouseMotionListener((MouseMotionListener) listener);
        listener = currentMode;
        addMouseListener((MouseListener) listener);
        addMouseMotionListener((MouseMotionListener) listener);
    }
    public boolean checkSelectedArea(Component component){
        //抓取物件的邊界
        Point upperleft = new Point(component.getX1(), component.getY1());
        Point lowerright = new Point(component.getX2(), component.getY2());
        //如果左上及右下皆在框內則被選取
        if (selectedArea.contains(upperleft) && selectedArea.contains(lowerright)) {
            return true;
        }
        return false;
    }
    public void groupComponent(){
        Group group = new Group();
        for(int i=0; i<allCanvasComponent.size();i++){
            Component component = allCanvasComponent.get(i);
            if(component.groupSelected){
                group.addComponent(component);
                allCanvasComponent.remove(component);
                i--;
            }
        }
        allCanvasComponent.add(group);
    }
    public void unGroupComponent(){
        Group group = (Group) selectedComponent;
        List<Component> groupComponents = (List<Component>) group.getGroupComponents();
        for(int i=0;i<groupComponents.size();i++){
            Component component = groupComponents.get(i);
            allCanvasComponent.add(component);
        }

        allCanvasComponent.remove(selectedComponent);

    }
    public void reset(){
        if(selectedComponent != null){
            selectedComponent.resetSelectedComp();
            selectedComponent = null;
        }

        selectedArea.setBounds(0,0,0,0);
    }



}
