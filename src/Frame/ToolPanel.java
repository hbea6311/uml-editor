package Frame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Button.*;

public class ToolPanel extends JPanel  {

    private JPanel toolPanel = new JPanel();
    private BaseButton holdBtn ;

    private HoldBtnListner holdBtnListner = new HoldBtnListner();
    private Canvas canvas = Canvas.getInstance();



    public ToolPanel(){

        JButton selectBtn = new SelectButton();
        selectBtn.addActionListener(holdBtnListner);
        JButton associationBtn = new AssociationButton() ;
        associationBtn.addActionListener(holdBtnListner);
        JButton generalBtn = new GeneralizeButton();
        generalBtn.addActionListener(holdBtnListner);
        JButton compBtn = new CompositionButton();
        compBtn.addActionListener(holdBtnListner);
        JButton classBtn = new ClassButton();
        classBtn.addActionListener(holdBtnListner);
        JButton usecaseBtn = new UseCaseButton();
        usecaseBtn.addActionListener(holdBtnListner);

        toolPanel.setLayout(new GridLayout(6,1));
        toolPanel.add(selectBtn);
        toolPanel.add(associationBtn);
        toolPanel.add(generalBtn);
        toolPanel.add(compBtn);
        toolPanel.add(classBtn);
        toolPanel.add(usecaseBtn);





    }
    public JPanel getPanel(){
        System.out.println("return panel");
        return toolPanel;
    }


    public class HoldBtnListner implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            //set previous holdbtn back to white
            if(holdBtn !=null) {
                holdBtn.setBackground(Color.white);
            }
            holdBtn= (BaseButton) e.getSource();
            //set new holdBtn to black
            if(holdBtn!=null){
                holdBtn.setBackground(Color.BLACK);

            }
            canvas.currentMode= holdBtn.mode;

            canvas.setCurrentMode();
            canvas.reset();
            canvas.repaint();

        }
    }
}

