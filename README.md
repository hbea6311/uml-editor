# UML editdor
為一個簡單UML編輯器，可以新增class, usecase，並在上面建立各種關係線，如Dependency, Aggregation等，用來練習java之物件導向觀念

<img src="https://i.imgur.com/W9icUTC.png" width="50%" height="50%">

下圖為整支程式的class diagram
![](https://i.imgur.com/tx0xpr8.png)
